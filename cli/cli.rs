use ncurses::{
    curs_set, delwin, endwin, getmaxyx, initscr, newwin, stdscr, wclear, wgetch, wprintw, wrefresh,
    CURSOR_VISIBILITY, WINDOW,
};

const PICO_URL: &str = "http://10.42.0.161/ctl";

const DTC1: u32 = 100;
const DTC2: u32 = 100;

pub struct Cli {
    state_w: WINDOW,
    input_w: WINDOW,

    dir1: i32,
    dtc1: u32,
    dir2: i32,
    dtc2: u32,

    shot: u32,
}

impl Cli {
    pub fn new() -> Self {
        let mut max_x: i32 = 0;
        let mut max_y: i32 = 0;

        initscr();
        curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
        getmaxyx(stdscr(), &mut max_y, &mut max_x);

        Cli {
            state_w: newwin(5, max_x, 0, 0),
            input_w: newwin(2, max_x, 6, 0),

            dir1: 0,
            dtc1: DTC1,
            dir2: 0,
            dtc2: DTC2,

            shot: 0,
        }
    }

    pub fn exit(&mut self) {
        delwin(self.state_w);
        delwin(self.input_w);
        endwin();
    }

    pub fn display(&self) {
        let dir1 = self.dir1;
        let dtc1 = self.dtc1;
        let dir2 = self.dir2;
        let dtc2 = self.dtc2;

        let shot = self.shot;

        wclear(self.state_w);
        wclear(self.input_w);

        wprintw(self.state_w, &format!("dir1: {dir1}\n"));
        wprintw(self.state_w, &format!("dtc1: {dtc1}\n"));
        wprintw(self.state_w, &format!("dir2: {dir2}\n"));
        wprintw(self.state_w, &format!("dtc2: {dtc2}\n"));
        wprintw(self.state_w, &format!("shot: {shot}\n"));
        wprintw(self.input_w, "[q/a/e/d] or [i/k/j] or [x]: ");

        wrefresh(self.state_w);
        wrefresh(self.input_w);
    }

    pub fn input(&self) -> char {
        wgetch(self.input_w) as u32 as u8 as char
    }

    fn error(&mut self) {
        self.exit();

        println!("error: could not connect to {PICO_URL}, exited.");

        std::process::exit(0);
    }

    pub fn reset(&mut self) {
        self.update('s');

        let req = format!("?dtc1={DTC1}&dtc2={DTC2}");

        match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
            Ok(_) => (),
            Err(_) => self.error(),
        }
    }

    pub fn update(&mut self, inp: char) {
        if inp == 'q' {
            let req = String::from("?dir1=0&pwm1=1");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            self.dir1 = 1;
        } else if inp == 'a' {
            let req = String::from("?dir1=1&pwm1=1");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            self.dir1 = -1;
        } else if inp == 'e' {
            let req = String::from("?dir2=0&pwm2=1");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            self.dir2 = 1;
        } else if inp == 'd' {
            let req = String::from("?dir2=1&pwm2=1");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            self.dir2 = -1;
        } else if inp == 's' {
            let req = String::from("?pwm1=0");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            let req = String::from("?dir1=0");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            let req = String::from("?pwm2=0");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            let req = String::from("?dir2=0");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            self.dir1 = 0;
            self.dir2 = 0;
        } else if inp == 'i' {
            let req = String::from("?aim=0");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }
        } else if inp == 'k' {
            let req = String::from("?aim=1");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }
        } else if inp == 'j' {
            let req = String::from("?shot");

            match reqwest::blocking::get(PICO_URL.to_owned() + &req) {
                Ok(_) => (),
                Err(_) => self.error(),
            }

            self.shot += 1;
        }
    }
}
