mod cli;

use crate::cli::Cli;

fn main() {
    let mut cli = Cli::new();

    cli.reset();
    cli.display();

    loop {
        let inp = cli.input();

        if inp == 'x' {
            break;
        }

        cli.update(inp);
        cli.display();
    }

    cli.reset();
    cli.exit();
}
