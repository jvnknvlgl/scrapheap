#include "aim.h"

void aim_setup(uint32_t dir) {
    add_alarm_in_ms(AIM_TIME_MS, aim_callback, NULL, false);

    pwm_setup(AIM1_1, AIM1_2, AIM_DTC, dir);
}

int64_t aim_callback(alarm_id_t id, void *priv) {
    pwm_close(AIM1_1, AIM1_2);

    return 0;
}
