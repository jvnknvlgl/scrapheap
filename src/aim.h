#ifndef AIM_H
#define AIM_H

#include <stdint.h>

#include "pico/stdlib.h"

#include "pwm.h"

#define AIM_TIME_MS 200
#define AIM_DTC 100

#define AIM1_1 0
#define AIM1_2 1

void aim_setup(uint32_t dir);

int64_t aim_callback(alarm_id_t id, void *priv);

#endif
