#include "ctl.h"

uint32_t ctl_setup() {
    static struct server *srv;
    static struct server_cfg cfg;

    static struct ctl_data ctl_data = {
        .pwm1 = false,
        .pwm2 = false,
        .dir1 = 0,
        .dir2 = 0,
        .dtc1 = 0,
        .dtc2 = 0,
    };

    cfg = http_default_cfg();

    err_t err = register_hndlr_methods(&cfg, "/ctl", ctl_hndlr,
                                       HTTP_METHODS_GET_HEAD, &ctl_data);

    if (err != ERR_OK) {
        printf("Failed to register /ctl: %d\n", err);

        return -1;
    }

    printf("Registered /ctl successfully.\n");

    while ((err = http_srv_init(&srv, &cfg)) != ERR_OK) {
        printf("Initializing server: %d\n", err);
    }

    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);

    printf("Server started.\n");

    return 0;
}

err_t ctl_hndlr(struct http *http, void *priv) {
    struct ctl_data *data;
    data = priv;

    size_t query_len;

    struct req *req = http_req(http);

    const uint8_t *query = http_req_query(req, &query_len);

    if (query != NULL) {
        http_req_query_iter(query, query_len, ctl_query, data);
    }

    return http_resp_err(http, HTTP_STATUS_IM_A_TEAPOT);
}

err_t ctl_query(const char *query, size_t query_len, const char *val,
                size_t val_len, void *priv) {
    struct ctl_data *data;
    data = priv;

    if (strncmp(query, "pwm1", sizeof("pwm1") - 1) == 0) {
        if (val[0] - '0' == 0) {
            data->pwm1 = false;

            pwm_close(PWM1_1, PWM1_2);
        } else {
            data->pwm1 = true;

            pwm_setup(PWM1_1, PWM1_2, data->dtc1, data->dir1);
        }
    } else if (strncmp(query, "pwm2", sizeof("pwm2") - 1) == 0) {
        if (val[0] - '0' == 0) {
            data->pwm2 = false;

            pwm_close(PWM2_1, PWM2_2);
        } else {
            data->pwm2 = true;

            pwm_setup(PWM2_1, PWM2_2, data->dtc2, data->dir2);
        }
    } else if (strncmp(query, "dtc1", sizeof("dtc1") - 1) == 0) {
        data->dtc1 =
            (100 * (val[0] - '0')) + (10 * (val[1] - '0')) + (val[2] - '0');

        if (data->pwm1) {
            pwm_setup(PWM1_1, PWM1_2, data->dtc1, data->dir1);
        }
    } else if (strncmp(query, "dtc2", sizeof("dtc2") - 1) == 0) {
        data->dtc2 =
            (100 * (val[0] - '0')) + (10 * (val[1] - '0')) + (val[2] - '0');

        if (data->pwm2) {
            pwm_setup(PWM2_1, PWM2_2, data->dtc2, data->dir2);
        }
    } else if (strncmp(query, "dir1", sizeof("dir1") - 1) == 0) {
        data->dir1 = val[0] - '0';

        if (data->pwm1) {
            pwm_setup(PWM1_1, PWM1_2, data->dtc1, data->dir1);
        }
    } else if (strncmp(query, "dir2", sizeof("dir2") - 1) == 0) {
        data->dir2 = val[0] - '0';

        if (data->pwm2) {
            pwm_setup(PWM2_1, PWM2_2, data->dtc2, data->dir2);
        }
    } else if (strncmp(query, "aim", sizeof("aim") - 1) == 0) {
        aim_setup(val[0] - '0');
    } else if (strncmp(query, "shot", sizeof("shot") - 1) == 0) {
        shot_setup();
    }
}
