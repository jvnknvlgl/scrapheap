#ifndef CTL_H
#define CTL_H

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "pico/cyw43_arch.h"
#include "pico/stdlib.h"

#include "picow_http/http.h"

#include "aim.h"
#include "pwm.h"
#include "shot.h"

struct ctl_data {
    bool pwm1;
    bool pwm2;
    uint32_t dir1;
    uint32_t dir2;
    uint32_t dtc1;
    uint32_t dtc2;
};

uint32_t ctl_setup();

err_t ctl_hndlr(struct http *http, void *priv);

err_t ctl_query(const char *query, size_t query_len, const char *val,
                size_t val_len, void *priv);

#endif
