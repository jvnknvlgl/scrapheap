#include <stdint.h>
#include <stdio.h>

#include "pico/stdlib.h"

#include "picow_http/http.h"

#include "ctl.h"
#include "shot.h"
#include "wlan.h"

uint32_t main() {
    stdio_init_all();

    sleep_ms(2000);

    if (wlan_setup()) {
        return -1;
    }

    gpio_init(SHOT1);
    gpio_set_dir(SHOT1, GPIO_OUT);

    gpio_put(SHOT1, 1);

    if (ctl_setup()) {
        return -1;
    }

    while (1) {
        sleep_ms(1000);
    }

    wlan_close();

    return 0;
}
