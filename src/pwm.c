#include "pwm.h"

void pwm_setup(uint32_t a, uint32_t b, uint32_t dtc, uint32_t dir) {
    gpio_set_function(a, GPIO_FUNC_PWM);
    gpio_set_function(b, GPIO_FUNC_PWM);

    uint32_t slice = pwm_gpio_to_slice_num(a);

    printf("Turning on PWM channel %d at %d, direction %d.\n", slice, dtc, dir);

    if (dir == 0) {
        pwm_set_freq_duty(slice, PWM_CHAN_A, PWM_FREQ, dtc);
        pwm_set_freq_duty(slice, PWM_CHAN_B, PWM_FREQ, 100);
        pwm_set_output_polarity(slice, true, false);
    } else {
        pwm_set_freq_duty(slice, PWM_CHAN_A, PWM_FREQ, 100);
        pwm_set_freq_duty(slice, PWM_CHAN_B, PWM_FREQ, dtc);
        pwm_set_output_polarity(slice, false, true);
    }

    pwm_set_enabled(slice, true);
}

void pwm_close(uint32_t a, uint32_t b) {
    uint32_t slice = pwm_gpio_to_slice_num(a);

    printf("Turning off PWM channel %d.\n", slice);

    pwm_set_freq_duty(slice, PWM_CHAN_A, PWM_FREQ, 0);
    pwm_set_freq_duty(slice, PWM_CHAN_B, PWM_FREQ, 0);
    pwm_set_output_polarity(slice, false, false);

    pwm_set_enabled(slice, true);
}

uint32_t pwm_set_freq_duty(uint32_t slice, uint32_t chan, uint32_t f,
                           uint32_t dtc) {
    uint32_t clk = 125000000;
    uint32_t div16 = clk / f / 4096 + (clk % (f * 4096) != 0);

    if (div16 / 16 == 0)
        div16 = 16;

    uint32_t wrap = clk * 16 / div16 / f - 1;

    pwm_set_clkdiv_int_frac(slice, div16 / 16, div16 & 0xF);
    pwm_set_wrap(slice, wrap);
    pwm_set_chan_level(slice, chan, wrap * dtc / 100);

    return wrap;
}
