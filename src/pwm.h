#ifndef PWM_H
#define PWM_H

#include <stdint.h>
#include <stdio.h>

#include "pico/stdlib.h"

#include "hardware/pwm.h"

#define PWM_FREQ 20000

#define PWM1_1 2
#define PWM1_2 3
#define PWM2_1 4
#define PWM2_2 5

void pwm_setup(uint32_t a, uint32_t b, uint32_t dtc, uint32_t dir);

void pwm_close(uint32_t a, uint32_t b);

uint32_t pwm_set_freq_duty(uint32_t slice, uint32_t chan, uint32_t f,
                           uint32_t dtc);

#endif
