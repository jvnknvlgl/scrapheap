#include "shot.h"

void shot_setup() {
    add_alarm_in_ms(SHOT_TIME_MS, shot_callback, NULL, false);

    gpio_put(SHOT1, 0);
}

int64_t shot_callback(alarm_id_t id, void *priv) {
    gpio_put(SHOT1, 1);

    printf("Fired a shot!\n");

    return 0;
}
