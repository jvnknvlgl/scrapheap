#ifndef SHOT_H
#define SHOT_H

#include <stdint.h>
#include <stdio.h>

#include "pico/stdlib.h"

#define SHOT_TIME_MS 10

#define SHOT1 6

void shot_setup();

int64_t shot_callback(alarm_id_t id, void *priv);

#endif
