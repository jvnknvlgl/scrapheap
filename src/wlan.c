#include "wlan.h"

uint32_t wlan_setup() {
    if (cyw43_arch_init()) {
        printf("Failed to initialize.\n");

        return -1;
    }

    cyw43_arch_enable_sta_mode();

    printf("Connecting to Wi-Fi... ");

    if (cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASS,
                                           CYW43_AUTH_WPA2_AES_PSK, 30000)) {
        printf("Failed to connect.\n");

        return -1;
    }

    printf("Connected.\n");

    return 0;
}

void wlan_close() { cyw43_arch_deinit(); }
