#ifndef WLAN_H
#define WLAN_H

#include <stdint.h>
#include <stdio.h>

#include "pico/cyw43_arch.h"
#include "pico/stdlib.h"

uint32_t wlan_setup();

void wlan_close();

#endif
