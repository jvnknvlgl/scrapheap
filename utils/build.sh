#!/bin/bash
mkdir -p build
cd build
cmake .. -DWIFI_SSID=scrapheap -DWIFI_PASS=scrapheap
make
cd ..
cargo build
