#!/bin/sh
apt update
apt install -y cmake gcc-arm-none-eabi gperf libnewlib-arm-none-eabi \
	libstdc++-arm-none-eabi-newlib python3-brotli python3-cryptography \
	python3-jinja2 python3-yaml

git submodule update --init
cd lib/pico-sdk
git submodule update --init
cd ../..
